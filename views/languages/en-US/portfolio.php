<?php
    define( 'WELCOME', 'Bienvenue !' );
    define( 'ABOUT_ME', 'A propos de moi' );
    define( 'SKILLS', 'Compétences' );
    define( 'EXPERIENCES', 'Expériences' );
    define( 'PROJECTS', 'Projets' );
    define( 'EDUCATIONS', 'Formations' );
    define( 'CONTACT_ME', 'Me contacter' );
    define( 'SOFTWARE', 'Logiciel' );
    define( 'WEB', 'Web' );
    define( 'OPERATING_SYSTEM', 'Système d\'exploitation' );
    define( 'DATABASE_OTHERS', 'Base de données et autres' );
    define( 'PORTFOLIO', 'Portfolio' );
    define( 'GITHUB_ME', 'Github - FlorentinDUBOIS' );
    define( 'TWITTER_ME', 'Twitter - @FlorentinDUBOIS' );
    define( 'FIRSTNAME', 'Prénom' );
    define( 'LASTNAME', 'Nom' );
    define( 'OUR_MAIL', 'Votre courriel' );
    define( 'TOOK_MESSAGE', 'Laissez-moi un message' );
    define( 'SEND', 'Envoyer' );
    define( 'SITEMAP', 'Carte du sîte' );
    define( 'LICENCE', 'Mentions légales' );
    define( 'GOOD_VISIT', 'Je vous souhaite une bonne visite sur mon portfolio.<br />N\'hésitez pas à me laisser un message' );
    define( 'COPYRIGHT', '© '.date( 'Y' ).' Copyright Florentin DUBOIS ' );
    define( 'ME', 'Bonjour !' );
    define( 'PORTFOLIO_TITLE', 'Florentin DUBOIS - Portfolio' );

    define( 'C_CPP', 'Ces deux langages sont ceux que je préfère avec le JavaScript, Ce sont aussi les premiers langages que j\'ai appris' );
    define( 'QT', 'Une magnifique bibliothèque multi-platforme que j\'utilise pour mes projets ayant besoin d\'une IHM' );
    define( 'SFML', 'Une autre bibliothèque multi-platforme que j\'utilise cette fois-ci avec la bibliothèque OpenGL' );
    define( 'OPENGL', 'Une bibliothèque multi-platforme graphique qui me permet de faire plein de triangles ^^' );
    define( 'JAVA', 'Langage sur lequel, j\'ai appris les design patterns notamment ceux du livre Gang of Four design patterns' );
    define( 'ASSEMBLY', 'J\'ai fait de l\'assembleur pour une carte ST7 à l\'ISEN' );
    define( 'PYTHON', 'Découvert en cours de phyique, pour modéliser les principes de la magnétostatique' );

    define( 'HTML', 'La base de n\'importe qu\'elle site web' );
    define( 'CSS', 'Sans le monde serai moin beau' );
    define( 'SASS', 'Langage de préprocessing vraiment très pratique et utile, personnellement je préfère son petit frère SCSS' );
    define( 'LESS', 'Langage de préprocessing intérréssant concurrant de SASS' );
    define( 'JAVASCRIPT', 'Un de mes langage préféré vraiment très pratique avec des concepts très intérréssant' );
    define( 'COFFEESCRIPT', 'Pour ce simplifier la vie, cependant ECMAScript 2015 comprends la plupart des apports de ce langage de préprocessing' );
    define( 'ANGULAR', 'Un framework JavaScript qui me passionne' );
    define( 'JQUERY', 'Un autre framework JavaScript incontournable' );
    define( 'NODE', 'L\'environnement JavaScript le plus connu' );
    define( 'EXPRESS', 'Un framework JavaScript pour Node' );
    define( 'JADE', 'Un langage de préprocessing html vraiment très pratique' );
    define( 'GULP', 'Gestionnaire de tâches géniale' );
    define( 'APACHE', 'Un vieux de la vieille' );
    define( 'PHP', 'Langage de préprocessing html très pratique, j\'affectionne beaucoup la version 7' );
    define( 'MATERIALIZE', 'Framework css basée sur le style material design que je trouve magnifique' );
    define( 'FOUNDATION', 'Framework css géniale comme base à un nouveau projet' );
    define( 'BOOTSTRAP', 'Plus besoin de le présenter ^^' );

    define( 'ARCHLINUX', 'Ma distribution Linux préféré elle m\'offre toujours les nouveautés dés que possible (rolling release) <br /><br />Elle suit aussi un de mes principes "KISS" (Keep It Simple, Stupid)' );
    define( 'UBUNTU', 'Comment passé à côté la distribution Linux la plus connu' );
    define( 'WINDOWS', 'Dure de ne pas connaître, le système d\'exploitation le plus utilisé sur les ordinateurs' );

    define( 'MONGODB', 'MongoDB est un projet NoSQL, auquelle je me suis intérréssé lorsque je découvrit l\'environnement Node.' );
    define( 'MYSQL', 'Principal Base de données utilisé, notamment pour sa gratuité' );
    define( 'MARIADB', 'Base de données sur laquel je fais la plupart de mes projets descendante de MySQL' );
    define( 'GIT', 'Gestionnaire de version bien utile pour retracer tout ce qui est fait, utilisé en adéquation avec <a href="http://github.com/FlorentinDUBOIS" >github</a>' );

    define( 'NAME_SANDWICH', 'Alternance à aC3' );
    define( 'DATE_SANDWICH', '1 octobre 2014 au 30 septembre 2015' );
    define( 'DESC_SANDWICH', 'Alternance durant l\'année scolaire 2014-2015 dans le cadre de la formation ISEN ainsi que l\'organisme "Union des Industries et Métiers de la Métallurgie" (abrégé UIMM) dans le but d\'obtenir le "Certificat de Qualification Paritaire de la Métallurgie" avec le titre "Chargé de projet informatiques et réseaux"' );
    define( 'NAME_STAGE', 'Stage à aC3' );
    define( 'DATE_STAGE', '16 juin 2014 au 29 âout 2014' );
    define( 'DESC_STAGE', 'Stage dit "technicien" dans le cadre de la formation ISEN. <br /><br />Ce stage a durée tout l\'été 2014, dans l\'entreprise aC3 dans le service s\'occupant de la maintenance et le développement de nouvelles fonctionnalités du logiciel' );

    define( 'NAME_ENGINEER', 'Diplôme d\'ingénieur' );
    define( 'DATE_ENGINEER', 'Prévu courant 2017' );
    define( 'DESC_ENGINEER', 'Diplôme qui me sera délivré en octobre 2017, ce sera un diplôme d\'ingénieur généraliste mais je me suis spécialiser (choix de la majeur) dans le domaine de l\'informatique, choix qui suit une continuité via la formation Cycle informatiques et Réseaux (CIR) de l\'ISEN Brest qui à précédé' );
    define( 'NAME_CQPM', 'Certificat de Qualification Paritaire de la Métallurgie' );
    define( 'DATE_CQPM', 'Septembre 2015' );
    define( 'DESC_CQPM', 'Certificat délivré par l\'organisme "Union des Industries et Métiers de la Métallurgie" (abrégé UIMM) en fin du Cycle Informatiques et Réseaux avec l\'intitulée "Chargé de projets informatiques et réseaux"' );
    define( 'NAME_CISCO', 'CISCO' );
    define( 'DATE_CISCO', 'Septembre 2013 - âout 2014' );
    define( 'DESC_CISCO', 'J\'ai suivi les cours des modules 1 et 2, j\'ai aussi suivi différents cours sur les principaux concepts des modules 3 et 4' );
    define( 'NAME_BAC', 'Baccalauréat scientifique' );
    define( 'DATE_BAC', 'Juillet 2012' );
    define( 'DESC_BAC', 'Baccalauréat scientifique obtenue, avec l\'option science de l\'ingénieur et la spécialité mathématiques' );

    define( 'NAME_FESTIGEEK', 'Festigeek' );
    define( 'DATE_FESTIGEEK', 'Prévu Décembre 2015' );
    define( 'DESC_FESTIGEEK', '' );
    define( 'NAME_PORTFOLIO', 'Portfolio' );
    define( 'DATE_PORTFOLIO', 'Novembre 2015' );
    define( 'DESC_PORTFOLIO', '' );
    define( 'NAME_SHELLCHOOSER', 'ShellChooser' );
    define( 'DATE_SHELLCHOOSER', 'Juillet 2015' );
    define( 'DESC_SHELLCHOOSER', '' );
    define( 'NAME_FRAMEWORK', 'Framework PHP' );
    define( 'DATE_FRAMEWORK', 'Âout 2014' );
    define( 'DESC_FRAMEWORK', '' );
?>
