<?php
    // ------------------------------------------------------------------------
    // define for this application
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // application define
    define( 'APPLICATION_NAME', 'v2.framework' );
    define( 'APPLICATION_ROBOT_INDEX', 'all' );
    define( 'APPLICATION_REVISIT_AFTER', '60' );
    define( 'APPLICATION_DATE_CREATION', date( 'm-d-Y' ));
    define( 'APPLICATION_COPYRIGHT', '' );
    define( 'APPLICATION_AUTHOR', 'Florentin DUBOIS' );
    define( 'APPLICATION_PUBLISHER', 'Florentin DUBOIS' );

    // ------------------------------------------------------------------------
    // application default define
    define( 'DEFAULT_CHARSET', 'utf-8' );
    define( 'DEFAULT_LANGUAGE', 'en-US' );
?>
