// -----------------------------------------------------------------------------
// configuration for beautify css
module.exports = {
    // -----------------------------------------------------------------------------
    // space to indent
    indent: "    ",

    // -----------------------------------------------------------------------------
    // open brace
    openbrace: "separate-line",

    // -----------------------------------------------------------------------------
    // append semicolon on last line of a rule
    autosemicolon: true
};
